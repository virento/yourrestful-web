'use strict';

export const SERVER_URL = "http://localhost:8200";
export const WEB_VERSION = "0.1";

// Basic API Values
export const API_GET_ALL = SERVER_URL + "/api";
export const API_GET_RECORD = SERVER_URL + "/api/";
export const API_POST_RECORD = SERVER_URL + "/api";
export const API_PUT_RECORD = SERVER_URL + "/api/";
export const API_STRUCTURE = SERVER_URL + "/api/structure"
export const API_PATCH_RECORD = SERVER_URL + "/api/";

//Authentication
export const AUTHENTICATION_REGISTER = SERVER_URL + "/register";
export const AUTHENTICATION_LOGIN = SERVER_URL + "/login";
export const VERIFICATION_EMAIL = SERVER_URL + "/verifyuser";
export const RESET_PASSWORD = SERVER_URL + "/resetPassword";
export const SUBMIT_REST_PASSWORD = SERVER_URL + "/newPassword";

// User
export const USER_INFO = SERVER_URL + "/profile";

// Resources
export const RESOURCE_LIST = SERVER_URL + "/user/resource";
export const RESOURCE_CREATE = SERVER_URL + "/user/resource";
export const RESOURCE_GET = SERVER_URL + "/user/resource";
export const RESOURCE_RECORDS_LIST = SERVER_URL + "/user";
export const RESOURCE_STRUCTURE_GET = SERVER_URL + "/user/{}/structure";
export const RESOURCE_CREATE_RECORD = SERVER_URL + "/user/{}";
export const RESOURCE_RECORD_DELETE = SERVER_URL + "/user/{}/{}";
export const RESOURCE_MODIFY = SERVER_URL + "/user/resource/{}";
export const RESOURCE_SHARE = SERVER_URL + "/user/{}/share";
export const RESOURCE_SHARE_LIST = SERVER_URL + "/user/{}/shares";
export const RESOURCE_SHARE_REMOVE = SERVER_URL + "/user/{}/share/{}";
export const RESOURCE_REMOVE = SERVER_URL + "/user/{}";
export const RESOURCE_GENERATE_RECORDS = SERVER_URL + "/user/{}/generate";

// Other
export const SYSTEM_PARAMETER = SERVER_URL + "/system/parameter/{}";
export const SYSTEM_PARAMETERS = SERVER_URL + "/system/parameter";
export const SYSTEM_USERS = SERVER_URL + "/system/users";
export const SYSTEM_USER = SERVER_URL + "/system/users/{}";

// Parameter
export const PARAMETER_MAX_RECOEDS = "MAX_RECORDS";
export const PARAMETER_MAX_RESOURCES = "MAX_RESOURCES";


export function parseUrl(url: string, values: string[]): string {
    values.forEach(element => {
        url = url.replace('{}', element);
    });
    return url;
}