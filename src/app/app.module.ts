import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainframeComponent } from './component/common/mainframe/mainframe.component';
import { MainPageComponent } from './component/main/main-page/main-page.component';
import { HeaderComponent } from './component/common/header/header.component';
import { FooterComponent } from './component/common/footer/footer.component';
import { LoginPageComponent } from './component/basic/login-page/login-page.component';
import { RegistrationPageComponent } from './component/basic/registration-page/registration-page.component';
import { AlertComponent } from './component/common/alert/alert.component';
import { HttpInterceptor } from './interceptors/http-interceptor';
import { DashboardComponent } from './component/dashboard/dashboard/dashboard.component';
import { ResourceListComponent } from './component/dashboard/resource-list/resource-list.component';
import { ResourceCreateComponent } from './component/dashboard/resource-create/resource-create.component';
import { ResourceDetailsComponent, ShareDialog } from './component/dashboard/resource-details/resource-details.component';
import { RecordsModifyComponent } from './component/dashboard/records-modify/records-modify.component';
import { RecordPipePipe } from './pipes/record-pipe.pipe';
import { DashboardBasicComponent } from './component/dashboard/dashboard-basic/dashboard-basic.component';
import { ProfilePageComponent } from './component/basic/profile-page/profile-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { SharesModifyComponent } from './component/dashboard/shares-modify/shares-modify.component';
import { PasswordResetComponent } from './component/basic/password-reset/password-reset.component';
import { GeneratorInformationComponent } from './component/basic/generator-information/generator-information.component';
import { ApiInformationComponent } from './component/basic/api-information/api-information.component';
import { AdminDashboardComponent } from './component/admin/admin-dashboard/admin-dashboard.component';
import { AdminParametersComponent, ParameterDialog } from './component/admin/admin-parameters/admin-parameters.component';
import { AdminUsersComponent, UserDialog } from './component/admin/admin-users/admin-users.component';
import { AdminDashboardBasicComponent } from './component/admin/admin-dashboard-basic/admin-dashboard-basic.component';

@NgModule({
  declarations: [
    AppComponent,
    MainframeComponent,
    MainPageComponent,
    HeaderComponent,
    FooterComponent,
    LoginPageComponent,
    RegistrationPageComponent,
    AlertComponent,
    DashboardComponent,
    ResourceListComponent,
    ResourceCreateComponent,
    ResourceDetailsComponent,
    RecordsModifyComponent,
    RecordPipePipe,
    DashboardBasicComponent,
    ProfilePageComponent,
    ShareDialog,
    ParameterDialog,
    UserDialog,
    SharesModifyComponent,
    PasswordResetComponent,
    GeneratorInformationComponent,
    ApiInformationComponent,
    AdminDashboardComponent,
    AdminParametersComponent,
    AdminUsersComponent,
    AdminDashboardBasicComponent
  ],
  entryComponents: [
    ShareDialog,
    ParameterDialog,
    UserDialog
  ],
  exports: [
    ShareDialog,
    ParameterDialog,
    UserDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TooltipModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
