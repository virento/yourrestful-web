import { PageInfo, PageInfoPlaceholder } from './page-info';

export interface Shares {
    content?: any;
    page?: PageInfo;
    links?: any;
}

export class SharesPlaceholder implements Shares {
    content = [];
    page = new PageInfoPlaceholder();
    linkgs = {};
}