import { PageInfo, PageInfoPlaceholder } from './page-info';

export interface Records {
    content?: any;
    page?: PageInfo;
    links?: any;
}

export class RecordsPlaceholder implements Records {
    content = [];
    page = new PageInfoPlaceholder();
    linkgs = {};
}