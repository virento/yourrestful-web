export interface PageInfo {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number
}

export class PageInfoPlaceholder implements PageInfo {
    size: null;
    totalElements: null;
    totalPages: null;
    number: null;
}
