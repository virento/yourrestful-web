import { LoginFormClass } from '../login-form';

export interface ResourceContent {
    active?: boolean,
    createDate?: number,
    editable?: boolean,
    links?: any,
    title?: string,
    url?: string
}
