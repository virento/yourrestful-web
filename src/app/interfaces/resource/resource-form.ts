import { FormGroup } from '@angular/forms';

export interface ResourceForm {
    title: String;
    active: Boolean;
    editable: Boolean;
    structure: {};
}

export class ResourceFormClass {
    public static toResourceForm(formGroup: FormGroup, resourceStructure: any): ResourceForm {
        const form = {} as ResourceForm;
        form.title = formGroup.value.titleInput
        form.active = formGroup.value.activeInput;
        form.editable = formGroup.value.editableInput;
        form.structure = resourceStructure;
        return form;
    }
}
