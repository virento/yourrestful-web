export interface Profile {
    email?: string;
    firstName?: string;
    lastName?: string;
    registrationDate?: Date;
    status?: string;
    type?: string;
    username?: string;
}

export class ProfilePlaceholder implements Profile {
    
}