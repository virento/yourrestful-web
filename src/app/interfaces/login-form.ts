import { FormGroup } from '@angular/forms';

export interface LoginForm {
    login: String,
    password: String,
}

export class LoginFormClass {
    public static toLoginForm(formGroup: FormGroup) : LoginForm {
        const form = {} as LoginForm;
        form.login = formGroup.value.loginInput;
        form.password = formGroup.value.passwordInput;
        return form;
    }
}