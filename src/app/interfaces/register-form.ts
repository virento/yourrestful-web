import { FormGroup } from '@angular/forms';

export interface RegisterForm {
    username: String;
    password: String;
    email: String;
    name: String;
}

export class RegisterFormClass {
    public static toRegisterForm(formGroup: FormGroup) : RegisterForm {
        const form = {} as RegisterForm;
        form.username = formGroup.value.usernameInput;
        form.password = formGroup.value.passwordInput;
        form.email = formGroup.value.emailInput;
        form.name = formGroup.value.nameInput;
        return form;
    }
}