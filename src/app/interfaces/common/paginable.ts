export interface Paginable {
    maxResult?: number;
    page?: number;
}
