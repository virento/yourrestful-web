import { ResourceRecord } from './resource-record';
import { Page } from './page';
import { ResourceContent } from './resource/resource-content';

export interface Resource {
    content?: ResourceContent,
    records?: ResourceRecord[],
    _links?: Object[],
    page?: Page,
}
