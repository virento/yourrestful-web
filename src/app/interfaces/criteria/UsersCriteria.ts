export interface UsersCriteria {
    email?: string;
    firstName?: string;
    lastName?: string;
    status?: string;
    type?: string;
    username?: string;
}

export class UsersCriteriaClass {
    public static createQuery(criteria: UsersCriteria): string {
        if (!criteria) {
            return "";
        }
        let query = "";
        if (criteria.email && criteria.email.length > 0) {
            query += "email=" + criteria.email + "&";
        }
        if (criteria.firstName && criteria.firstName.length > 0) {
            query +=  "name=" + criteria.firstName + "&";
        }
        if (criteria.lastName && criteria.lastName.length > 0) {
            query += "surname=" + criteria.lastName + "&";
        }
        if (criteria.status && criteria.status.length > 0) {
            query += "status=" + criteria.status + "&";
        }
        if (criteria.type && criteria.type.length > 0) {
            query += "type=" + criteria.type + "&";
        }
        if (criteria.username && criteria.username.length > 0) {
            query += "username=" + criteria.username + "&";
        }

        return query.length > 0 ? "?" + query.substring(0, query.length - 1) : "";
    }
}
