import { PageInfoPlaceholder } from './resource/page-info';

export class ParametersPlaceholder {
    content = [];
    page = new PageInfoPlaceholder();
    linkgs = {};
}