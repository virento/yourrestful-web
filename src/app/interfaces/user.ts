export interface UserToken extends User{
    token: string;
    refreshToken: string;
    expireDate: Date;
}

export interface User {
    username?: string;
    registrationDate?: Date;
    firstName?: string;
    lastName?: string;
    email?: string;
    status?: string;
    type?: string;
}
