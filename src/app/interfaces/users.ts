import { PageInfoPlaceholder } from './resource/page-info';

export class UsersPlaceholder {
    content = [];
    page = new PageInfoPlaceholder();
    linkgs = {};
}