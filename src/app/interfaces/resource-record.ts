export interface ResourceRecord {
    id: Number,
    _links: Object[]
}
