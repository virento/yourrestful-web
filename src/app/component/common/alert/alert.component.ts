import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  errorMessage: String;
  successMessage: String;
  errorFields: any[];

  constructor() { }

  ngOnInit() {
  }

  setErrororMessage(msg: String) {
    this.errorMessage = msg;
    this.successMessage = null;
    this.errorFields = null;
  }

  setErrorMessageWithFields(msg: String, description: String, fields: any[]) {
    let errorMessage = "[" + msg + "] " + description;
    this.errorMessage = errorMessage;
    this.errorFields = fields;
  }

  setSuccessMessage(msg: String) {
    this.successMessage = msg;
    this.errorMessage = null;
  }

  clearMessage() {
    this.errorMessage = null;
    this.successMessage = null;
    this.errorMessage = null;
  }

}
