import { Component, OnInit } from '@angular/core';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';
import { UserToken } from 'src/app/interfaces/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: UserToken;

  constructor(
    private authenticationService: AuthenitcationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(user => this.user = user);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/home']);
    return false;
  }
}
