import { Component, OnInit } from '@angular/core';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';

@Component({
  selector: 'app-mainframe',
  templateUrl: './mainframe.component.html',
  styleUrls: ['./mainframe.component.css']
})
export class MainframeComponent implements OnInit {

  constructor(
    private authenticationService: AuthenitcationService
  ) {
    this.authenticationService.prepareCurrentUser()
  }

  ngOnInit() {

  }

}
