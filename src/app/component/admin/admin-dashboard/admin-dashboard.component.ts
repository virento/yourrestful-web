import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/nav-service.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  constructor(
    private navService: NavService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.navService.pushNavLink([]);
  }


}
