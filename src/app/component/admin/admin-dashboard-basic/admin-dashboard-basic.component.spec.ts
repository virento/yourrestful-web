import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashboardBasicComponent } from './admin-dashboard-basic.component';

describe('AdminDashboardBasicComponent', () => {
  let component: AdminDashboardBasicComponent;
  let fixture: ComponentFixture<AdminDashboardBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashboardBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
