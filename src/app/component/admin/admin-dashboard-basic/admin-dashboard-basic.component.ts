import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/nav-service.service';
import { NavLink } from '../../dashboard/dashboard-basic/nav-link';

@Component({
  selector: 'app-admin-dashboard-basic',
  templateUrl: './admin-dashboard-basic.component.html',
  styleUrls: ['./admin-dashboard-basic.component.css']
})
export class AdminDashboardBasicComponent implements OnInit {

  navs: NavLink[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.navService.pushSubject$.subscribe(data => {
      if (data === null) return;
      this.navs = data;
      this.cdr.detectChanges();
    })
  }
}
