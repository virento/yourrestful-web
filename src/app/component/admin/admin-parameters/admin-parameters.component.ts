import { Component, Inject, OnInit } from '@angular/core';
import { Paginable } from 'src/app/interfaces/common/paginable';
import { PageInfo } from 'src/app/interfaces/resource/page-info';
import { ParametersPlaceholder } from 'src/app/interfaces/ParametersPlaceholder';
import { SystemServiceService } from 'src/app/services/system-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ViewChild } from '@angular/core';
import { AlertComponent } from '../../common/alert/alert.component';
import { NavService } from 'src/app/services/nav-service.service';

@Component({
  selector: 'app-admin-parameters',
  templateUrl: './admin-parameters.component.html',
  styleUrls: ['./admin-parameters.component.css']
})
export class AdminParametersComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  private paginable: Paginable = { page: 0, maxResult: 12 };
  pageInfo: PageInfo = null;
  parameters: any = new ParametersPlaceholder();

  private criteriaCode = null;

  constructor(
    private systemService: SystemServiceService,
    private dialog: MatDialog,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.navService.pushNavLink([{ link: 'admin/parameters', name: 'Parameters' }]);

    let input: any = document.getElementById('parameter-code');
    let timeout = null;

    input.addEventListener('keyup', e => {
      clearTimeout(timeout);

      timeout = setTimeout(() => {
        this.criteriaCode = input.value.length > 0 ? input.value : null;
        this.systemService.findParameters(this.criteriaCode, this.paginable).subscribe(data => {
          this.parameters = data;
          this.pageInfo = this.parameters.page;
        })
      }, 100);
    });


    this.systemService.findParameters(null, this.paginable).subscribe(data => {
      this.parameters = data;
      this.pageInfo = this.parameters.page;
    });
  }

  changePage(foreward) {
    let page = this.paginable.page;
    if (foreward && (this.paginable.page + 1) < this.pageInfo.totalPages) {
      this.paginable.page++;
    } else if (!foreward && this.paginable.page > 0) {
      this.paginable.page--;
    }
    if (this.paginable.page == page) {
      return;
    }
    this.systemService.findParameters(null, this.paginable).subscribe(data => {
      this.parameters = data;
      this.pageInfo = this.parameters.page;
    });
  }

  onParameterClick(code: string) {
    const dialogRef = this.dialog.open(ParameterDialog, {
      width: '500px',
      data: { code: code, value: this.getParameterValue(code) }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        return;
      }
      this.systemService.modifyParameter(data.code, data.value).subscribe(respones => {
        this.alert.setSuccessMessage("Value for parameter " + data.code + " has been changed");
        this.systemService.findParameters(this.criteriaCode, this.paginable).subscribe(data => {
          this.parameters = data;
          this.pageInfo = this.parameters.page;
        });
      }, error => {
        this.alert.setErrororMessage("Error while changing parameter value");
      })
    });
  }

  getParameterValue(code: string) {
    for (let i = 0; i < this.parameters.content.length; i++) {
      const element = this.parameters.content[i];
      if (element.code === code) {
        return element.value;
      }
    }
  }

}

export interface ParameterDialogData {
  code: string;
  value: string;
}

@Component({
  selector: 'parameter-dialog',
  styleUrls: ['parameter-dialog.css'],
  templateUrl: 'parameter-dialog.html'
})
export class ParameterDialog {
  constructor(
    public dialogRef: MatDialogRef<ParameterDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ParameterDialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
