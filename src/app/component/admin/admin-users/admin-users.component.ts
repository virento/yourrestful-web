import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Paginable } from 'src/app/interfaces/common/paginable';
import { UsersCriteria } from 'src/app/interfaces/criteria/UsersCriteria';
import { PageInfo } from 'src/app/interfaces/resource/page-info';
import { User, UserToken } from 'src/app/interfaces/user';
import { UsersPlaceholder } from 'src/app/interfaces/users';
import { NavService } from 'src/app/services/nav-service.service';
import { SystemServiceService } from 'src/app/services/system-service.service';
import { AlertComponent } from '../../common/alert/alert.component';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  private paginable: Paginable = { page: 0, maxResult: 12 };
  pageInfo: PageInfo = null;
  users: any = new UsersPlaceholder();

  private criteria: UsersCriteria = {};

  constructor(
    private systemService: SystemServiceService,
    private dialog: MatDialog,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.navService.pushNavLink([{ link: 'admin/users', name: 'Users' }]);
    this.addSearchKeyListener('firstName', 'firstName');
    this.addSearchKeyListener('lastName', 'lastName');
    this.addSearchKeyListener('username', 'username');
    this.addSearchKeyListener('email', 'email');
    this.addSearchSelectListener('type', 'type');
    this.addSearchSelectListener('status', 'status');

    this.systemService.findUsers(this.criteria).subscribe(data => {
      this.users = data;
      this.pageInfo = data.page;
    });
  }
  addSearchSelectListener(id: string, fieldName: string) {
    let input: any = document.getElementById(id);

    input.addEventListener('change', () => {
      this.criteria[fieldName] = input.value;
      this.systemService.findUsers(this.criteria).subscribe(data => {
        this.users = data;
        this.pageInfo = data.page;
      });
    });
  }

  addSearchKeyListener(id, fieldName) {
    let input: any = document.getElementById(id);
    let timeout = null;

    input.addEventListener('keyup', e => {
      clearTimeout(timeout);

      timeout = setTimeout(() => {
        let value = input.value.length > 0 ? input.value : null;
        this.criteria[fieldName] = value;
        this.systemService.findUsers(this.criteria).subscribe(data => {
          this.users = data;
          this.pageInfo = data.page;
        });
      }, 100);
    });
  }

  changePage(foreward) {
    let page = this.paginable.page;
    if (foreward && (this.paginable.page + 1) < this.pageInfo.totalPages) {
      this.paginable.page++;
    } else if (!foreward && this.paginable.page > 0) {
      this.paginable.page--;
    }
    if (this.paginable.page == page) {
      return;
    }
    this.systemService.findUsers(this.criteria).subscribe(data => {
      this.users = data;
      this.pageInfo = data.page;
    });
  }

  onUserClick(username: string) {
    const dialogRef = this.dialog.open(UserDialog, {
      width: '500px',
      data: this.getUserData(username)
    })
    dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        return;
      }
      this.systemService.modifyUser(data).subscribe(response => {
        this.alert.setSuccessMessage("User with username " + username + " has been modified");
        this.systemService.findUsers(this.criteria).subscribe(data => {
          this.users = data;
          this.pageInfo = data.page;
        });
      }, error => {
        if (error.error.errorCode === 'VALIDATION_ERROR') {
          this.alert.setErrorMessageWithFields(error.error.errorCode, error.error.description, error.error.errors);
        } else {
          this.alert.setErrororMessage('[' + error.error.errorCode + "] " + error.error.description);
        }
      });
    })
  }

  getUserData(username: string): User {
    for (let i = 0; i < this.users.content.length; i++) {
      const element = this.users.content[i];
      if (element.username === username) {
        return {
          email: element.email,
          firstName: element.firstName,
          lastName: element.lastName,
          status: element.status,
          type: element.type,
          username: element.username
        };
      }
    }
  }

}

@Component({
  selector: 'user-dialog',
  styleUrls: ['user-dialog.css'],
  templateUrl: 'user-dialog.html'
})
export class UserDialog {
  constructor(
    public dialogRef: MatDialogRef<UserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: User) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}