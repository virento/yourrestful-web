import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router, NavigationEnd } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Observable } from 'rxjs';
import { RecordsCreate } from 'src/app/extras/editors/records-create';
import { ResourceService } from 'src/app/services/resource.service';
import { AlertComponent } from '../../common/alert/alert.component';
import * as Globals from '../../../global';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;
  @ViewChild('urlInput', { static: true }) urlInput: any;

  activeRoute: String = 'get';
  private subscription: any;
  // private response: any;
  // private response2: any;
  private links: any;
  private url: any;
  urlMethod: string;

  private createRecord: RecordsCreate = new RecordsCreate();
  private resourceStructure;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService
  ) {
  }

  ngOnInit() {
    this.route.url.subscribe(() => {
      if (this.route.firstChild !== null && this.route.firstChild.routeConfig !== undefined) {
        this.activeRoute = this.route.firstChild.routeConfig.path;
        console.log('Path: ' + this.activeRoute);
      }
    })
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd && this.activeRoute !== this.route.firstChild.routeConfig.path) {
        if (this.subscription) {
          this.subscription.unsubscribe();
        }
        this.activeRoute = this.route.firstChild.routeConfig.path;
        this.subscription = this.route.firstChild.url.subscribe(x => {
          this.activeRoute = x[0].path;
          this.reloadApiContent();
          this.alert.clearMessage();
        })
      }
    })
    this.api.getResourceStructure().subscribe(data => {
      this.resourceStructure = JSON.parse(data.body.structure);
      this.createRecord.setCreateRecordCallback(this.addRecord);
      this.createRecord.initialize(this.resourceStructure, 'newRecord');
      if (this.activeRoute === 'new') {
        this.choseNew();
        this.reloadApiContent();
      } else if (this.activeRoute === 'edit') {
        this.choseEdit();
        this.reloadApiContent();
      }
    })
    if (this.activeRoute !== 'new' && this.activeRoute !== 'edit') {
      this.reloadApiContent();
    }
  }

  addRecord() {
    this.alert.setErrororMessage(null);
    let record = this.createRecord.getInputsAsObject();
    let endpoint;
    if (this.activeRoute === 'new') {
      endpoint = this.api.postRecord(record);
    } else if (this.activeRoute === 'edit') {
      endpoint = this.api.editRecord(record);
    }
    endpoint.subscribe({
      next: data => {
        if (this.activeRoute == 'edit') {
          this.alert.setSuccessMessage("Successfuly edited record with id " + record.id + ".");
          delete data.links;
          document.getElementById('responseEdit2').innerHTML = this.syntaxHighlight(JSON.stringify(data, undefined, 4));
        } else if (this.activeRoute === 'new') {
          delete data.links;
          document.getElementById('response').innerHTML = this.syntaxHighlight(JSON.stringify(data, undefined, 4));
          this.alert.setSuccessMessage("Successfuly create record with id " + record.id + ".");
        }
      }, error: error => {
        this.alert.setErrorMessageWithFields(error.error.errorCode, error.error.description, error.error.errors);
        // this.alert.setErrororMessage("test<br> fdsafs")
      }
    });
  }

  private reloadApiContent() {
    let observable: Observable<any>;
    this.createRecord.hideForm();
    switch (this.activeRoute) {
      case null:
      case 'get-all':
        this.urlMethod = "GET";
        this.urlInput.nativeElement.value = Globals.API_GET_ALL;
        observable = this.api.getAllRecords();
        break;
      case 'new':
        this.urlInput.nativeElement.value = Globals.API_POST_RECORD;
        this.urlMethod = "POST";
        let response: any = document.getElementById('response');
        if (response) {
          response.innerHTML = null;
        }
        this.choseNew();
        break;
      case 'edit':
        this.urlMethod = "PATCH";
        this.urlInput.nativeElement.value = Globals.API_PATCH_RECORD;
        this.choseEdit();
        break;
      case 'get':
        this.urlMethod = "GET";
        this.urlInput.nativeElement.value = Globals.API_GET_RECORD + 400;
        observable = this.api.getRecord(400);
    }
    if (observable === undefined) return;
    observable.subscribe((data) => {
      if (data.content !== undefined) {
        data.content.forEach(element => {
          delete element.links;
        });
      }
      delete data.links;
      document.getElementById('response').innerHTML = this.syntaxHighlight(JSON.stringify(data, undefined, 4));
    })
  }

  private choseNew() {
    this.alert.clearMessage();
    this.createRecord.enable("id");
    this.createRecord.clearValues();
    this.createRecord.showForm();
    // this.response = undefined;
  }

  private choseEdit() {
    this.alert.clearMessage();
    this.api.getRecord(400).subscribe((data: any) => {
      delete data.links;
      document.getElementById('responseEdit').innerHTML = this.syntaxHighlight(JSON.stringify(data, undefined, 4));
    })
    this.createRecord.showForm();
    this.createRecord.setValue("id", 400);
    this.createRecord.disable("id");
  }

  private syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
      var cls = 'darkorange';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'red';
        } else {
          cls = 'green';
        }
      } else if (/true|false/.test(match)) {
        cls = 'blue';
      } else if (/null/.test(match)) {
        cls = 'magenta';
      }
      return '<span style="color:' + cls + ';">' + match + '</span>';
    });
  }
}
