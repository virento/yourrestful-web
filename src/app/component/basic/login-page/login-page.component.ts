import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertComponent } from '../../common/alert/alert.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';
import { LoginFormClass } from 'src/app/interfaces/login-form';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  isRequested = false;

  loginForm: FormGroup;

  private returnUrl: String;

  constructor(
    private authenticationService: AuthenitcationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.url.subscribe(params => {
      if (params[0].path === 'verify') {
        this.activatedRoute.queryParams.subscribe(params => {
          let token = params['token'];
          let username = params['username'];
          this.authenticationService.verifyEmail(token, username).subscribe(data => {
            this.alert.setSuccessMessage("Email verified. You can now log in to your account.");
          });
        });
      }
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params['returnUrl'];
    });
    this.loginForm = new FormGroup({
      loginInput: new FormControl(''),
      passwordInput: new FormControl('')
    });
    if (history.state.data !== undefined) {
      if (history.state.data.registered !== undefined) {
        this.alert.setSuccessMessage("Registered. Please click on activation link that was send to your email.");
      } else if(history.state.data.newPassword !== undefined) {
        this.alert.setSuccessMessage("Password changed. You can now log in");
      }
    }
  }

  login() {
    this.alert.setErrororMessage(null);
    var loginForm = LoginFormClass.toLoginForm(this.loginForm);
    this.isRequested = true;
    this.authenticationService.loginUser(loginForm).subscribe({
      next: data => {
        let redirectUrl = this.returnUrl;
        if (redirectUrl !== undefined && redirectUrl !== null) {
          this.router.navigate([redirectUrl]);
        } else {
          this.router.navigate(['/dashboard']);
        }
        this.isRequested = false;
      },
      error: err => {
        this.alert.setErrororMessage(err.error.description)
        this.isRequested = false;
      }
    });
  }

}
