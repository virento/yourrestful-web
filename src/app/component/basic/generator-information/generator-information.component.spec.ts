import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratorInformationComponent } from './generator-information.component';

describe('GeneratorInformationComponent', () => {
  let component: GeneratorInformationComponent;
  let fixture: ComponentFixture<GeneratorInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratorInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
