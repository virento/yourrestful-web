import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';
import { AlertComponent } from '../../common/alert/alert.component';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  resetForm: FormGroup;
  newPasswordForm: FormGroup;

  isRequestPage = false;

  isRequested = false;

  private token: string;
  private username: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenitcationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.url.subscribe(params => {
      if (params[0].path === 'resetPassword') {
        this.isRequestPage = true;
      } else {
        this.activatedRoute.queryParams.subscribe(params => {
          this.token = params['token'];
          this.username = params['username'];
          if (!this.token || !this.username) {
            this.router.navigate(['/login']);
          }
        });
      }
    });

    this.resetForm = new FormGroup({
      emailInput: new FormControl('', [Validators.required, Validators.email])
    });

    this.newPasswordForm = new FormGroup({
      passwordInput: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatPasswordInput: new FormControl('', [Validators.required, Validators.minLength(6)]),
    }, { validators: [this.passwordValidator()] })
  }

  resetPassword() {
    if (!this.resetForm.valid) {
      this.alert.setErrororMessage('Reset password form contains problem');
      Object.keys(this.resetForm.controls).forEach(key => {
        this.resetForm.controls[key].markAsTouched();
      })
      return;
    } else {
      this.alert.setErrororMessage(null);
    }

    this.isRequested = true;
    this.authenticationService.resetPassword(this.resetForm.value.emailInput).subscribe(data => {
      this.alert.setSuccessMessage('Reset link has been sent to your email');
      this.isRequested = false;
    }, err => {
      this.alert.setErrororMessage(err.error.description);
      this.isRequested = false;
    });
  }

  submitNewPassword() {
    if (!this.newPasswordForm.valid) {
      this.alert.setErrororMessage('Reset password form contains problem');
      Object.keys(this.newPasswordForm.controls).forEach(key => {
        this.newPasswordForm.controls[key].markAsTouched();
      })
      return;
    } else {
      this.alert.setErrororMessage(null);
    }

    this.isRequested = true;
    this.authenticationService.submitNewPassword(this.token, this.username, this.newPasswordForm.value.passwordInput).subscribe(data => {
      this.isRequested = false;
      this.router.navigate(['/login'], { state: { data: { newPassword: true } } });
    }, err => {
      this.alert.setErrororMessage(err.error.description);
      this.isRequested = false;
    });
  }

  passwordValidator(): ValidatorFn {
    return (ctrl: AbstractControl): ValidationErrors => {
      const group = ctrl as FormGroup;
      const passwd = group.controls['passwordInput'];
      const passwdRepeat = group.controls['repeatPasswordInput'];
      if (passwd.value === passwdRepeat.value) {
        return null;
      }
      return { passwdnotequal: true };
    };
  }

}
