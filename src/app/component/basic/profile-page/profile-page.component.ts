import { Component, OnInit } from '@angular/core';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';
import { Profile, ProfilePlaceholder } from 'src/app/interfaces/profile';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  profile: Profile = new ProfilePlaceholder();

  constructor(
    private authenticationService: AuthenitcationService
  ) { }

  ngOnInit() {
    this.authenticationService.getProfile().subscribe(data => {
      this.profile = data.body;
    })
  }

}
