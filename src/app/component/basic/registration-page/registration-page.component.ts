import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl, FormBuilder } from '@angular/forms';
import { AlertComponent } from '../../common/alert/alert.component';
import { AuthenitcationService } from 'src/app/services/authenitcation.service';
import { RegisterFormClass } from '../../../interfaces/register-form';
import { Router } from '@angular/router';
import { ObjectUnsubscribedError } from 'rxjs';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {

  @ViewChild('alert', { static: true }) alert: AlertComponent;

  isRequested = false;

  registerForm: FormGroup;

  loading = false;

  constructor(
    private authenticationService: AuthenitcationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      usernameInput: new FormControl('', [Validators.required, Validators.minLength(5)]),
      emailInput: new FormControl('', [Validators.required, Validators.email]),
      passwordInput: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatPasswordInput: new FormControl('', [Validators.required, Validators.minLength(6)]),
      nameInput: new FormControl(''),
    },
      { validators: [this.passwordValidator()] });
  }

  passwordValidator(): ValidatorFn {
    return (ctrl: AbstractControl): ValidationErrors => {
      const group = ctrl as FormGroup;
      const passwd = group.controls['passwordInput'];
      const passwdRepeat = group.controls['repeatPasswordInput'];
      if (passwd.value === passwdRepeat.value) {
        return null;
      }
      return { passwdnotequal: true };
    };
  }

  register() {
    if (!this.registerForm.valid) {
      this.alert.setErrororMessage('Registration form contains problem');
      Object.keys(this.registerForm.controls).forEach(key => {
        this.registerForm.controls[key].markAsTouched();
      })
      return;
    } else {
      this.alert.setErrororMessage(null);
    }
    const form = RegisterFormClass.toRegisterForm(this.registerForm);
    this.isRequested = true;
    this.authenticationService.registerUser(form).subscribe({
      next: data => {
        this.isRequested = false;
        this.router.navigate(['/login'], { state: { data: { registered: true } } });
      },
      error: err => {
        this.isRequested = false;
        this.alert.setErrororMessage(err.error.description)
      }
    });
  }

}
