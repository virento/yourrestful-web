import { Component, OnInit } from '@angular/core';
import * as Globals from '../../../global';

@Component({
  selector: 'app-api-information',
  templateUrl: './api-information.component.html',
  styleUrls: ['./api-information.component.css']
})
export class ApiInformationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getGlobal() {
    return Globals;
  }

}
