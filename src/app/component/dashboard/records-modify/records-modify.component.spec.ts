import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordsModifyComponent } from './records-modify.component';

describe('RecordsModifyComponent', () => {
  let component: RecordsModifyComponent;
  let fixture: ComponentFixture<RecordsModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordsModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordsModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
