import { Component, OnInit, ViewChild } from '@angular/core';
import { Records, RecordsPlaceholder } from 'src/app/interfaces/resource/records';
import { ResourceService } from 'src/app/services/resource.service';
import { PageInfo } from 'src/app/interfaces/resource/page-info';
import { Paginable } from 'src/app/interfaces/common/paginable';
import { ActivatedRoute } from '@angular/router';
import { Resource } from 'src/app/interfaces/resource';
import { RecordsCreate } from 'src/app/extras/editors/records-create';
import { AlertComponent } from '../../common/alert/alert.component';
import { NavService } from 'src/app/services/nav-service.service';

declare var $: any;

@Component({
  selector: 'app-records-modify',
  templateUrl: './records-modify.component.html',
  styleUrls: ['./records-modify.component.css']
})
export class RecordsModifyComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  records: Records = new RecordsPlaceholder();
  pageInfo: PageInfo = null;
  private paginable: Paginable = { page: 0, maxResult: 12 };
  private resource: Resource;

  private resourceUrl;
  private createRecord: RecordsCreate = new RecordsCreate();
  private resourceStructure: {};

  constructor(
    private resourceService: ResourceService,
    private route: ActivatedRoute,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.resourceUrl = this.route.snapshot.paramMap.get('id');
    this.resourceService.getResource(this.resourceUrl).subscribe(data => {
      this.navService.pushNavLink([
        { link: 'resource/' + this.resourceUrl, name: 'Resource (' + data.body.title + ')' },
        { link: 'records/' + this.resourceUrl + "/modify", name: 'Modify records' }]);
    })

    $('#buttonAddRecord').hide();
    this.resourceService.getResourceRecords(this.resourceUrl, this.paginable).subscribe(data => {
      this.records = data.body;
      this.pageInfo = this.records.page;
    });
    this.resourceService.getResourceStructure(this.resourceUrl).subscribe(data => {
      this.resourceStructure = JSON.parse(data.body.structure);
      this.createRecord.initialize(this.resourceStructure, 'newRecord');
      this.createRecord.setCreateRecordCallback(this.addRecord);
    })
  }

  addRecord() {
    this.alert.setErrororMessage(null);
    let record = this.createRecord.getInputsAsObject();
    this.resourceService.postNewResourceRecord(this.resourceUrl, record).subscribe({
      next: body => {
        this.alert.setSuccessMessage("Successfuly create record with id " + record.id + ".");
        this.createRecord.clearValues();
        this.createRecord.hideForm();
        $('#buttonAddRecord').hide();
        this.resourceService.getResourceRecords(this.resourceUrl, this.paginable).subscribe(data => {
          this.records = data.body;
          this.pageInfo = this.records.page;
        });
      }, error: error => {
        this.alert.setErrorMessageWithFields(error.error.errorCode, error.error.description, error.error.errors);
      }
    });
  }

  changePage(foreward) {
    let page = this.paginable.page;
    if (foreward && (this.paginable.page + 1) < this.pageInfo.totalPages) {
      this.paginable.page++;
    } else if (!foreward && this.paginable.page > 0) {
      this.paginable.page--;
    }
    if (this.paginable.page == page) {
      return;
    }
    this.resourceService.getResourceRecords(this.resourceUrl, this.paginable).subscribe(data => {
      this.records = data.body;
      this.pageInfo = this.records.page;
    });
  }

  onCreateClick() {
    this.createRecord.showForm();
    $('#buttonAddRecord').show();
  }

  removeRecord(id) {
    this.resourceService.removeRecord(this.resourceUrl, id).subscribe(data => {
      this.alert.setSuccessMessage('Record with id [' + id + '] has been removed')
      this.resourceService.getResourceRecords(this.resourceUrl, this.paginable).subscribe(data => {
        this.records = data.body;
        this.pageInfo = this.records.page;
      });
    });
  }

}
