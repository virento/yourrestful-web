import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertComponent } from '../../common/alert/alert.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StructureEditor } from 'src/app/extras/editors/structure-editor';
import { ResourceService } from 'src/app/services/resource.service';
import { ResourceFormClass } from 'src/app/interfaces/resource/resource-form';
import { NavService } from 'src/app/services/nav-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceContent } from 'src/app/interfaces/resource/resource-content';
import { ResourceStructure } from 'src/app/interfaces/resource/resource-structure';
import { Generator } from 'src/app/extras/editors/generator';
import { SystemServiceService } from 'src/app/services/system-service.service';
import * as Globals from '../../../global';

declare var $: any;

@Component({
  selector: 'app-resource-create',
  templateUrl: './resource-create.component.html',
  styleUrls: ['./resource-create.component.css']
})
export class ResourceCreateComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;
  @ViewChild('structure', { static: true }) structure;

  // MODIFY
  isModify: boolean = false;
  private resourceUrl: string;
  resource: ResourceContent;
  private resourceStructure: ResourceStructure[] = [];

  // MODIFY END

  createForm: FormGroup;

  private structureEditor: StructureEditor;

  showRecordsForm: boolean = false;

  activeOption: string = 'noRecords';

  private generator: Generator = new Generator();

  maxRecords: number;

  constructor(
    private resourceService: ResourceService,
    private navService: NavService,
    private route: ActivatedRoute,
    private router: Router,
    private systemService: SystemServiceService
  ) { }

  ngAfterViewInit() {
    if (this.resourceUrl === null) {
      this.navService.pushNavLink([{ link: 'resource/new', name: 'New Resource' }]);
    }
  }

  async ngOnInit() {
    this.createForm = new FormGroup({
      titleInput: new FormControl('', [Validators.required, Validators.minLength(5)]),
      editableInput: new FormControl(false,),
      activeInput: new FormControl(true,)
    })
    this.structureEditor = new StructureEditor(this.structure);

    this.resourceUrl = this.route.snapshot.paramMap.get('id');
    if (this.resourceUrl !== null) {
      this.structureEditor.init(true);
      this.isModify = true;
      this.resourceService.getResourceStructure(this.resourceUrl).subscribe(data => {
        const structure = JSON.parse(data.body.structure);
        console.log(structure)
        this.structureEditor.setStructureForm(structure);
        this.structureEditor.refreshStructureTree();
        this.structureEditor.disable();
      });
      this.loadExistingResource();
    } else {
      // this.navService.pushNavLink([{ link: 'resource/new', name: 'New Resource' }]);
      this.structureEditor.init();
      this.systemService.getParameterValue("STRUCTURE_MAX_FIELDS").subscribe(data => this.structureEditor.setMaxField(data)); 
      this.systemService.getParameterValue("STRUCTURE_MAX_NESTING").subscribe(data => this.structureEditor.setMaxNest(data)); 
    }
  }

  private loadExistingResource() {
    this.resourceService.getResource(this.resourceUrl).subscribe(data => {
      this.resource = data.body;
      this.fillResource(this.resource);
      this.navService.pushNavLink([
        { link: 'resource/' + this.resourceUrl, name: 'Resource (' + this.resource.title + ')' },
        { link: 'resource/' + this.resourceUrl + '/modify', name: 'Modify' }
      ]);
    });
  }

  fillResource(resource: ResourceContent) {
    this.createForm.setValue({ titleInput: resource.title, editableInput: resource.editable, activeInput: resource.active });
  }

  create() {
    if (this.valid('titleInput') && !this.showRecordsForm) {
      this.showRecordsForm = true;
      this.structureEditor.disable();
      return;
    }
    if (!this.createForm.valid) {
      this.alert.setErrororMessage('Resource form contains problem');
      Object.keys(this.createForm.controls).forEach(key => {
        this.createForm.controls[key].markAsTouched();
      })
      return;
    }
    let generate = null;
    if (this.generator.isInitialized() && document.getElementById('recordsGenerate') !== null) {
      generate = {
        content: this.generator.getInputsAsObject(),
        generate: (document.getElementById('recordsGenerate') as HTMLInputElement).valueAsNumber
      };
    }

    this.resourceService.postNewResource(ResourceFormClass.toResourceForm(this.createForm, this.structureEditor.getStructureForm), generate).subscribe(
      data => {
        this.router.navigate(['/dashboard', 'resource', data.body.url], { state: { data: { created: true, generated: data.body.generated } } });
      },
      error => {
        this.alert.setErrororMessage('[' + error.error.errorCode + '] ' + error.error.description);
        $(window).scrollTop(0);
      }
    );
  }

  valid(formName: string): boolean {
    return this.createForm.controls[formName].valid;
  }

  recordOptionChange(name: string) {
    this.activeOption = name;
    if (name === 'createRecords') {
      this.systemService.getParameterValue(Globals.PARAMETER_MAX_RECOEDS).subscribe(data => {
        this.maxRecords = data;
      });
      if (!this.generator.isInitialized()) {
        this.generator.initialize(JSON.parse(JSON.stringify(this.structureEditor.getStructureForm)), 'generator');
      }
      this.generator.showForm();
    } else {
      this.generator.hideForm();
    }
  }

  save() {
    this.resourceService.patchResource(this.resourceUrl, ResourceFormClass.toResourceForm(this.createForm, this.structureEditor.getStructureForm))
      .subscribe(
        data => {
          this.router.navigate(['/dashboard', 'resource', data.body.url], { state: { data: { modified: true } } });
        },
        err => {
          this.alert.setErrororMessage(err.error.description)
        })
  }


}
