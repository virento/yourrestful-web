import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharesModifyComponent } from './shares-modify.component';

describe('SharesModifyComponent', () => {
  let component: SharesModifyComponent;
  let fixture: ComponentFixture<SharesModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharesModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharesModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
