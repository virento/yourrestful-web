import { Inject, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Paginable } from 'src/app/interfaces/common/paginable';
import { PageInfo } from 'src/app/interfaces/resource/page-info';
import { Shares, SharesPlaceholder } from 'src/app/interfaces/resource/shares';
import { NavService } from 'src/app/services/nav-service.service';
import { ResourceService } from 'src/app/services/resource.service';
import { AlertComponent } from '../../common/alert/alert.component';
import { ShareDialog, ShareDialogData } from '../resource-details/resource-details.component';

declare var $: any;

@Component({
  selector: 'app-shares-modify',
  templateUrl: './shares-modify.component.html',
  styleUrls: ['./shares-modify.component.css']
})
export class SharesModifyComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  shares: Shares = new SharesPlaceholder();
  pageInfo: PageInfo = null;
  private paginable: Paginable = { page: 0, maxResult: 12 };

  private resourceUrl;

  constructor(
    private resourceService: ResourceService,
    private route: ActivatedRoute,
    private navService: NavService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.resourceUrl = this.route.snapshot.paramMap.get('id');
    this.resourceService.getResource(this.resourceUrl).subscribe(data => {
      this.navService.pushNavLink([
        { link: 'resource/' + this.resourceUrl, name: 'Resource (' + data.body.title + ')' },
        { link: 'records/' + this.resourceUrl + "/share/modify", name: 'Modify shares' }
      ]);
    })

    this.refreshResourceShare();
  }

  private refreshResourceShare() {
    this.resourceService.getResourceShares(this.resourceUrl, this.paginable).subscribe(data => {
      this.shares = data.body;
      this.pageInfo = this.shares.page;
    });
  }

  changePage(foreward) {
    let page = this.paginable.page;
    if (foreward && this.paginable.page < this.pageInfo.totalPages) {
      this.paginable.page++;
    } else if (this.paginable.page > 0) {
      this.paginable.page--;
    }
    if (this.paginable.page == page) {
      return;
    }
    this.resourceService.getResourceRecords(this.resourceUrl, this.paginable).subscribe(data => {
      this.shares = data.body;
      this.pageInfo = this.shares.page;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(ShareDialog, {
      width: '300px',
      data: { name: '' }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        return;
      }
      this.resourceService.postResourceShare(this.resourceUrl, data, data).subscribe({
        next: success => {
          this.refreshResourceShare();
          this.alert.setSuccessMessage('Resource has been shared with [' + data + ']')
        },
        error: err => {
          this.alert.setErrororMessage(err.error.description);
        }
      })
    })
  }

  removeRecord(item) {
    this.resourceService.removeUserShare(this.resourceUrl, item).subscribe(data => {
      this.alert.setSuccessMessage('You no longer share this resource with [' + item + ']')
      this.resourceService.getResourceShares(this.resourceUrl, { maxResult: 5 }).subscribe(data => {
        this.shares = data.body;
      });
    });
  }

}
