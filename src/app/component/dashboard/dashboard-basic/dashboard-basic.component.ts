import { Component, OnInit } from '@angular/core';
import { NavLink } from './nav-link';
import { NavService } from 'src/app/services/nav-service.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-dashboard-basic',
  templateUrl: './dashboard-basic.component.html',
  styleUrls: ['./dashboard-basic.component.css']
})
export class DashboardBasicComponent implements OnInit {

  navs: NavLink[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.navService.pushSubject$.subscribe(data => {
      if (data === null) return;
      this.navs = data;
      this.cdr.detectChanges();
    })
  }
}
