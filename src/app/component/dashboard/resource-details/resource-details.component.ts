import { Component, OnInit, Output, EventEmitter, Inject, ViewChild } from '@angular/core';
import { ResourceService } from 'src/app/services/resource.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Records, RecordsPlaceholder } from 'src/app/interfaces/resource/records';
import { PageInfo } from 'src/app/interfaces/resource/page-info';
import { ResourceContent } from 'src/app/interfaces/resource/resource-content';
import { NavService } from 'src/app/services/nav-service.service';
import { Shares, SharesPlaceholder } from 'src/app/interfaces/resource/shares';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlertComponent } from '../../common/alert/alert.component';

@Component({
  selector: 'app-resource-details',
  templateUrl: './resource-details.component.html',
  styleUrls: ['./resource-details.component.css']
})
export class ResourceDetailsComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  resource: ResourceContent = {};
  records: Records = new RecordsPlaceholder();
  shares: Shares = new SharesPlaceholder();
  pageInfo: PageInfo = null;
  private resourceUrl: string;

  constructor(
    private resourceService: ResourceService,
    private route: ActivatedRoute,
    private navService: NavService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.resourceUrl = this.route.snapshot.paramMap.get('id');
    this.resourceService.getResource(this.resourceUrl).subscribe(data => {
      this.resource = data.body;
      this.addLinks();
    });
    this.resourceService.getResourceRecords(this.resourceUrl, { maxResult: 5 }).subscribe(data => {
      this.records = data.body;
      console.log(this.records);
    })
    this.refreshResourceShare();
    if (history.state.data !== undefined) {
      if (history.state.data.created !== undefined) {
        let msg = "Resource has been succesfully created";
        if (history.state.data.generated !== undefined) {
          msg = msg + ". " + history.state.data.generated + " records has been generated";
        }
        this.alert.setSuccessMessage(msg);
      }
      if (history.state.data.modified !== undefined) {
        this.alert.setSuccessMessage('Resource successfully modified!');
      }
    }
  }

  private refreshResourceShare() {
    this.resourceService.getResourceShares(this.resourceUrl, { maxResult: 5 }).subscribe(data => {
      this.shares = data.body;
    });
  }

  deleteResource() {
    let isTrue = confirm("Remove " + this.resourceUrl + " resource?");
    if (isTrue) {
      this.resourceService.removeResource(this.resourceUrl).subscribe({
        next: success => {
          this.router.navigate(['/dashboard'], { state: { data: { deleted: true } } });
        },
        error: err => {
          this.alert.setErrororMessage(err.error.description);
        }
      });
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(ShareDialog, {
      width: '300px',
      data: { name: '' }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        return;
      }
      this.resourceService.postResourceShare(this.resourceUrl, data, data).subscribe({
        next: success => {
          this.refreshResourceShare();
          this.alert.setSuccessMessage('Resource has been shared with [' + data + ']')
        },
        error: err => {
          this.alert.setErrororMessage(err.error.description);
        }
      })
    })
  }

  addLinks() {
    this.navService.pushNavLink([{ link: 'resource/' + this.resourceUrl, name: 'Resource (' + this.resource.title + ")" }]);
  }
}

export interface ShareDialogData {
  name: string;
}

@Component({
  selector: 'share-dialog',
  templateUrl: 'share-dialog.html'
})
export class ShareDialog {
  constructor(
    public dialogRef: MatDialogRef<ShareDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ShareDialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
