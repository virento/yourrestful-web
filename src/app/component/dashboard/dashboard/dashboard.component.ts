import { Component, OnInit, ViewChild } from '@angular/core';
import { ResourceService } from 'src/app/services/resource.service';
import { NavService } from 'src/app/services/nav-service.service';
import { AlertComponent } from '../../common/alert/alert.component';
import { SystemServiceService } from 'src/app/services/system-service.service';
import { ThrowStmt } from '@angular/compiler';
import * as Globals from '../../../global'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('alert', { static: true }) alert: AlertComponent;

  resources;
  maxResource;

  constructor(
    private resourceService: ResourceService,
    private navService: NavService,
    private systemService: SystemServiceService
  ) { }

  ngAfterViewInit() {
    this.navService.pushNavLink([]);
  }


  ngOnInit() {
    // this.navService.pushNavLink([]);
    this.resourceService.getResourceList({ maxResult: 5 }).subscribe(result => {
      this.resources = result.body.content;
    });
    this.systemService.getParameterValue(Globals.PARAMETER_MAX_RESOURCES).subscribe(result => {
      this.maxResource = result;
    });

    if (history.state.data !== undefined) {
      if (history.state.data.deleted !== undefined) {
        this.alert.setSuccessMessage("Resource has been deleted");
      }
    }
  }

}
