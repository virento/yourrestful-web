import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { first } from 'rxjs/operators';
import { Resource } from 'src/app/interfaces/resource';

declare var $: any;

export class RecordsCreate {

    protected readonly INPUT_PREFIX = "record_";
    protected div;
    private callback: (record) => void;
    protected inputs: any[] = [];

    protected structureArrays = new Map();

    private resource: Resource;


    private arraysNames: string[] = [];

    RecordsCreate() {
    }

    isInitialized() {
        return this.resource !== undefined;
    }

    initialize(resource: Resource, div: string) {
        this.resource = resource;
        this.div = $('#' + div);
        this.createForm();
    }

    createForm() {
        let contentDiv = document.createElement('div');
        this.inputs = [];
        $(contentDiv).hide();
        contentDiv.style.marginTop = '10px';
        contentDiv.style.minWidth = '400px';
        this.fixStructure();
        this.fillStructureDiv(contentDiv, this.resource, null);
        $(this.div).replaceWith(contentDiv);
        this.div = contentDiv;
    }
    fixStructure() {
        this.structureArrays.forEach((value, key) => {
            let arrayNameSplice = key.split('.');
            let obj = this.resource;
            for (let i = 0; i < arrayNameSplice.length - 1; i++) {
                obj = obj[arrayNameSplice];
            }
            let newArray = [];
            newArray.push(value);
            obj[arrayNameSplice[arrayNameSplice.length - 1]] = newArray;
        })
        this.structureArrays.clear()
    }

    private fillStructureDiv(contentDiv: HTMLDivElement, structure, subkey) {
        for (const [key, value] of Object.entries(structure)) {
            if (Array.isArray(value)) {
                this.arraysNames.push(key);
                let masterDiv = document.createElement('div');
                $(contentDiv).append(masterDiv);
                let arrayDiv = document.createElement('div');
                masterDiv.append(this.createLabel(key + ': ['));
                masterDiv.id = key;
                arrayDiv.style.marginLeft = masterDiv.style.marginLeft === '' ? '2em' : (masterDiv.style.marginLeft.substring(0, 1) + 'em');
                this.fillStructureDiv(arrayDiv, value, subkey ? subkey + "." + key : key);
                $(masterDiv).append(arrayDiv);
                const addObjectButton = this.getButtonAddObjectToArrayTag(arrayDiv, value);
                addObjectButton.style.marginLeft = arrayDiv.style.marginLeft;
                $(masterDiv).append(addObjectButton);
                $(masterDiv).append(this.createLabel(']'));
                $(masterDiv).append(document.createElement('br'));
            } else if (typeof value === 'object') {
                let masterDiv = document.createElement('div');
                $(contentDiv).append(masterDiv);
                let objectDiv = document.createElement('div');
                let label: any = this.createLabel(key + ": {")
                if (Array.isArray(structure)) {
                    label = this.createRemoveIcon(key + ': {', masterDiv, subkey, structure);
                }
                masterDiv.append(label);
                masterDiv.id = key;
                objectDiv.style.marginLeft = masterDiv.style.marginLeft === '' ? '2em' : (masterDiv.style.marginLeft.substring(0, 1) + 'em');
                objectDiv.style.marginBottom = '-0.5em';
                this.fillStructureDiv(objectDiv, value, subkey ? subkey + "." + key : key);
                $(masterDiv).append(objectDiv);
                masterDiv.append(this.createLabel("}"));
                masterDiv.append(document.createElement('br'));
            } else {
                this.addInput(contentDiv, subkey ? subkey + "." + key : key, value + '');
            }
        }
    }
    createRemoveIcon(value: string, masterDiv, key, structure): any {
        let div = document.createElement('div');
        div.style.display = 'table';
        div.innerHTML = value + " ";
        let anchor = document.createElement('a');
        $(anchor).addClass('icon-remove');
        anchor.style.color = 'red';
        anchor.style.fontSize = 'x-large';
        anchor.style.display = 'table-cell';
        anchor.style.verticalAlign = 'middle';
        anchor.style.paddingLeft = '5px';
        anchor.style.cursor = 'pointer';
        anchor.addEventListener('click', (e) => {
            this.removeObjectFromArray(masterDiv, key);
            let tempObject = null;
            if (structure.length === 1) {
                tempObject = structure[0];
                // this.structureArrays.set(key, structure[0]);
                structure.splice(0, 1);
            } else {
                structure.shift();
            }
            let inputs = this.inputs;
            this.createForm();
            inputs.forEach(input => {
                const key = input.id.substring(this.INPUT_PREFIX.length);
                let value = input.value;
                if (input.type === 'checkbox') {
                    value = input.checked;
                }
                this.setValue(key, value);
            })
            this.showForm();
            if (tempObject !== null) {
                this.structureArrays.set(key, tempObject)
            }
        })
        div.append(anchor);
        return div;
    }

    removeObjectFromArray(masterDiv, prefix) {
        $(masterDiv).remove();
        this.removeInput(this.INPUT_PREFIX + prefix + '.' + masterDiv.id)
        this.fixArrayInputs(this.INPUT_PREFIX + prefix, Number(masterDiv.id));
    }

    removeInput(startsWith: string) {
        for (const [key, value] of Object.entries(this.inputs)) {
            if (value.id.startsWith(startsWith)) {
                delete this.inputs[key];
            }
        }
    }

    fixArrayInputs(prefix: string, id: number) {
        var offset = 1;
        var toFix = new Map();
        var found = true;
        while (found) {
            found = false;
            for (const [key, value] of Object.entries(this.inputs)) {
                if (value.id.startsWith(prefix + '.' + (id + offset))) {
                    this.addAllInputs(toFix, id + offset - 1, prefix + '.' + (id + offset));
                    found = true;
                    break;
                }
            }
            offset++;
        }
        for (let [key, value] of toFix.entries()) {
            value.forEach(value => {
                this.removeInput(value);
                var newId = prefix + "." + key + "." + value.id.substring(prefix.length + 1).split('.', 2)[1];
                console.log(value.id + " -> " + newId);
                value.id = newId;
                this.inputs.push(value);
            })
        }
    }

    addAllInputs(array: Map<string, any>, newId: number, prefix: string) {
        let objects = [];
        for (const [key, value] of Object.entries(this.inputs)) {
            if (value.id.startsWith(prefix)) {
                objects.push(value);
            }
        }
        array.set(String(newId), objects)
    }


    getButtonAddObjectToArrayTag(arrayDiv: HTMLDivElement, value: any[]): any {
        let buttonDiv = document.createElement('div');
        $(buttonDiv).addClass('row');
        let button = document.createElement('button');
        $(button).addClass('btn btn-secondary');
        button.innerText = 'Add object';
        button.addEventListener('click', (e) => {
            e.preventDefault();
            value.push(value[0]);
            let inputs = this.inputs;
            this.createForm();
            inputs.forEach(input => {
                const key = input.id.substring(this.INPUT_PREFIX.length);
                let value = input.value;
                if (input.type === 'checkbox') {
                    value = input.checked;
                }
                this.setValue(key, value);
            })
            $(this.div).show();
        })
        $(buttonDiv).append(button);
        return buttonDiv;
    }

    copyObjectWithoutValue(object) {
        let newObject = {};
        for (const [key, value] of Object.entries(object)) {
            if (typeof value == 'object') {
                newObject[key] = this.copyObjectWithoutValue(value);
            } else {
                newObject[key] = value;
            }
        }
        return newObject;
    }

    addInput(div, key: string, value: string) {
        const row = document.createElement('div');
        $(row).addClass('row');
        $(row).addClass('form-group');
        row.style.marginBottom = '0.5em';
        row.style.minWidth = '450px';
        const label = document.createElement('label');
        let object;
        // $(label).addClass('col-sm-3');
        // $(label).addClass('col-form-label');
        label.style.textAlign = 'right';
        $(label).text(key.substring(key.lastIndexOf('.') + 1) + ":");
        $(label).prop('for', this.INPUT_PREFIX + key)
        switch (value.toLowerCase()) {
            case 'long':
            case 'double':
                object = this.addInputForm(key, 'number');
                break;
            case 'text':
                object = this.addInputForm(key, 'text');
                break;
            case 'boolean':
                object = this.addInputForm(key, 'checkbox');
                break;
            case 'date':
                row.style.marginLeft = '0';
                const dateTimeDiv = document.createElement('div');
                dateTimeDiv.id = this.INPUT_PREFIX + key;
                const dateInput = this.addInputForm(key + "-date", 'date');
                dateTimeDiv.append(dateInput);
                const timeInput = this.addInputForm(key + "-time", 'time');
                timeInput.style.marginLeft = '10px';
                timeInput.style.minHeight = '30px';
                dateInput.style.minHeight = '30px';
                dateTimeDiv.append(timeInput);
                object = dateTimeDiv;
                break;
            default:
                console.error('type: ' + value + ' not supported');
                break;
        }
        $(row).append(label);
        object.style.maxWidth = '300px';
        if (value.toLowerCase() !== 'date' && value.toLowerCase() !== 'boolean') {
            $(object).addClass('form-control');
            $(object).addClass('form-control-sm');
        }
        let inputDiv = document.createElement('div');
        $(inputDiv).addClass('col-sm-9');
        $(inputDiv).append(object);
        $(row).append(inputDiv);
        $(div).append(row);
    }

    addInputForm(key, inputType): any {
        const input = document.createElement('input');
        this.inputs.push(input);
        $(input).prop('id', this.INPUT_PREFIX + key)
        $(input).prop('type', inputType);
        return input;
    }

    showForm() {
        $(this.div).show();
    }

    hideForm() {
        $(this.div).hide();
    }

    disable(inputName) {
        this.inputs.forEach(element => {
            if (element.id.substring(this.INPUT_PREFIX.length) === inputName) {
                element.disabled = true;
            }
        })
    }

    enable(inputName: string) {
        this.inputs.forEach(element => {
            if (element.id.substring(this.INPUT_PREFIX.length) === inputName) {
                element.disabled = false;
            }
        })
    }

    setValue(inputName, value) {
        this.inputs.forEach(element => {
            if (element.id.substring(this.INPUT_PREFIX.length) === inputName) {
                if (element.type === 'checkbox') {
                    element.checked = value;
                } else {
                    element.value = value;
                }
            }
        })
    }

    clearValues() {
        this.inputs.forEach(element => {
            element.value = "";
        });

        this.normalizeStructure(this.resource);
        this.createForm();
    }

    normalizeStructure(object: {}) {
        if (!object) {
            return;
        }
        for (const [key, value] of Object.entries(object)) {
            if (Array.isArray(value)) {
                object[key] = value.slice(0, 1);
            }
            if (typeof value === 'object') {
                this.normalizeStructure(value);
            }
        }
    }

    setCreateRecordCallback(callback: (record) => void) {
        this.callback = callback;
    }

    private getCreateButton() {
        let button = document.createElement('button');
        $(button).text('Add record');
        $(button).addClass('btn');
        $(button).addClass('btn-primary');
        button.addEventListener('click', () => { this.callback(this.getInputsAsObject()) });
        return button;
    }
    getInputsAsObject(): any {
        let object = {};
        this.inputs.forEach(element => {
            let key = element.id;
            let value = element.value;
            if (element.type === 'text') {
                if (value === '') value = null;
            } else if (element.type == 'number') {
                value = element.valueAsNumber;
            } else if (element.type == 'checkbox') {
                value = element.checked;
            } else if (element.type == 'date') {
                let timeObject = this.getInputWithName(key.substring(0, key.length - 'date'.length) + 'time');
                let timeObjectValue = timeObject.value.split(':');
                value = element.value.replaceAll('-', '/') + ' ' + timeObjectValue[0] + ":" + timeObjectValue[1] + ':00';
                key = key.substring(0, key.length - '-date'.length);
            } else if (element.type == 'time') {
                return; //ignore time is added when date element is added
            }
            this.putObjectToMap(object, key.substring(this.INPUT_PREFIX.length), value);
        });
        return object;
    }

    getInputStartsWith(inputName: string) {
        let input: any;
        for (input in this.inputs) {
            input = this.inputs[input];
            if (input.id.startsWith(inputName)) {
                return input;
            }
        }
    }

    getInputWithName(inputName: any): any {
        let input: any;
        for (input in this.inputs) {
            input = this.inputs[input];
            if (input.id === inputName) {
                return input;
            }
        }
    }

    putObjectToMap(map, key, value) {
        let keyDelimeterIndex = key.indexOf('.');
        let firstKey = keyDelimeterIndex === undefined ? key : key.substring(0, keyDelimeterIndex);
        if (this.arraysNames.includes(firstKey)) {
            let leftKey = key.substring(keyDelimeterIndex + 1);
            if (map[firstKey] === undefined) {
                map[firstKey] = [];
            }
            let indexNumber = leftKey.substring(0, leftKey.indexOf('.'));
            leftKey = leftKey.substring(leftKey.indexOf('.') + 1);
            if (map[firstKey][indexNumber] === undefined) {
                map[firstKey][indexNumber] = {};
            }
            this.putObjectToMap(map[firstKey][indexNumber], leftKey, value);
            return;
        }
        if (keyDelimeterIndex === -1) {
            map[key] = value;
            return;
        }
        let leftKey = key.substring(keyDelimeterIndex + 1);
        if (map[firstKey] === undefined) {
            map[firstKey] = {};
        }
        this.putObjectToMap(map[firstKey], leftKey, value);
    }

    createLabel(name: string) {
        const tag = document.createElement('label');
        tag.innerText = name;
        return tag;
    }
}
