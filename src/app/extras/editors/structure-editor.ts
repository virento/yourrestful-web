import { ArrayDataSource } from '@angular/cdk/collections';
import { ResourceStructure } from 'src/app/interfaces/resource/resource-structure';

declare var $: any;

export class StructureEditor {

    // private structureForm: ResourceStructure[] = [];

    private structureForm: any = {};

    private maxNumOfFields: number = 100;
    private maxNestLevel: number = 100;

    get getStructureForm(): any {
        return this.structureForm;
    }

    setStructureForm(form: any) {
        this.structureForm = form;
    }

    constructor(content) {
    }

    public init(disabled?: boolean) {
        let div = $('#structure');
        const object = this.getObjectStructure(disabled);
        div.append(object);
    }

    private getObjectStructure(disabled: boolean) {
        const div = document.createElement('div');
        div.append(this.createLabel("{"));
        div.append(this.getStructureContentDiv());
        if (!disabled) {
            div.append(this.getStructureInputRow());
        }
        div.append(this.createLabel("}"));
        return div;
    }

    createLabel(name: string) {
        const tag = document.createElement('label');
        tag.innerText = name;
        return tag;
    }

    getStructureContentDiv() {
        let structureContent = document.createElement('div');
        structureContent.id = 'structure-content';
        return structureContent;
    }

    resourceAddNewObjectButton(nativeElement: any) {
        document.createElement('button');
        console.log($('#structure'));
        let button = document.createElement('button');
        button.innerText = 'Create';
        $('#structure').append(button);
    }

    getStructureInputRow(): any {
        let row = document.createElement('div');
        row.id = 'structureRowInput'
        row.style.marginLeft = '2em';
        let inputDiv = document.createElement('div');
        let inputText = this.getStructureObjectNameInput();
        let button = this.getStructureAddButton();
        let select = this.getStructureObjectTypeSelect();
        button.addEventListener('click', (e) => {
            e.preventDefault();
            let input: any = inputText;
            let currentSpan = $(row).find('span')[0];
            if (input.value === null || input.value.length === 0) {
                if (!currentSpan) {
                    let span = this.getInputErrorSpan();
                    $(row).append(span);
                }
                return true;
            }
            $(currentSpan).remove();
            let objname = inputText.parentElement.parentElement.parentElement.getAttribute('id');
            this.addStructureNode(inputText, select, objname);
        });
        $(inputDiv).addClass('row');
        $(inputDiv).addClass('form-inline');
        $(inputDiv).append(inputText);
        $(inputDiv).append(select);
        $(inputDiv).append(button);
        $(row).append(inputDiv);
        return row;
    }

    private getInputErrorSpan() {
        let span = document.createElement('span');
        span.innerText = 'Field required';
        span.style.color = 'red';
        return span;
    }

    private getStructureObjectNameInput() {
        let inputText = document.createElement('input');
        $(inputText).addClass('form-control');
        inputText.id = 'structureInputName';
        inputText.placeholder = 'Enter object name';
        return inputText;
    }

    private getStructureObjectTypeSelect() {
        let select = document.createElement('select')
        $(select).addClass('form-control');
        select.style.marginLeft = "10px";
        $(select).addClass('structure-select');
        const options = [
            { name: 'Text', value: 'text' },
            { name: 'Number', value: 'long' },
            { name: 'Double', value: 'double' },
            { name: 'Logical', value: 'boolean' },
            { name: 'Object', value: 'object' },
            { name: 'Date', value: 'date' },
            { name: 'Object array', value: 'array' }
        ];
        options.forEach(option => {
            let tag = document.createElement('option');
            tag.setAttribute('value', option.value);
            tag.innerText = option.name;
            select.append(tag);
        })
        return select;
    }

    private getStructureAddButton() {
        let button = document.createElement('a');
        button.innerText = 'add';
        $(button).addClass('nav-link');
        button.type = 'Button';
        button.setAttribute('href', '#');
        return button;
    }

    addStructureNode(inputText: HTMLInputElement, select: HTMLSelectElement, objectId: string) {
        let object = this.getObjectFromStructure(objectId);
        if (Array.isArray(object)) {
            object = object[0];
        }
        if (select.value === 'object') {
            object[inputText.value] = {};
        } else if (select.value === 'array') {
            object[inputText.value] = [];
            object[inputText.value][0] = {};
        } else {
            object[inputText.value] = select.value;
        }


        this.refreshStructureTree();
        inputText.value = "";
    }

    getObjectFromStructure(objectId: string) {
        if (objectId == null) {
            return this.structureForm;
        }
        let names = objectId.split('.');
        let object = this.structureForm;
        for (let i = 0; i < names.length; i++) {
            if (object[names[i]] === undefined) {
                object[names[i]] = {};
            }
            object = object[names[i]];
        }
        return object;
    }

    refreshStructureTree() {
        let oldContent = $('#structure-content');
        let content = this.getStructureContentDiv();
        oldContent.replaceWith(content);
        this.setInputVisible(Object.entries(this.structureForm).length + 1, $('[id="structureRowInput"]')[$('[id="structureRowInput"]').length - 1]);
        this.fillObjectTree(content, this.structureForm, null);
    }

    private fillObjectTree(content: HTMLDivElement, form: any, parentId) {
        for (const [key, value] of Object.entries(form)) {
            let div = document.createElement('div');
            let titleDiv = document.createElement('div');
            titleDiv.style.display = 'table';
            titleDiv.style.marginBottom = '0.5rem';
            // let nameLabel = document.createElement('label');
            // let naleDiv = this.createLabelWithRemoveButton(key);
            div.style.marginLeft = '2em';
            // nameLabel.innerHTML = '"' + key + '":&nbsp;';
            let valueLabel = value + '';
            titleDiv.append('"' + key + '": ');
            // div.append(this.createRemoveIcon());
            // div.append(nameLabel);
            // div.append(naleDiv);
            div.append(titleDiv);
            if (Array.isArray(value)) {
                titleDiv.append("[");
                const arrayStructure = this.getStructureContentDiv();
                this.fillObjectTree(arrayStructure, value[0], parentId == null ? key : parentId + "." + key);
                div.id = parentId == null ? key : parentId + "." + key;
                let level = Number(div.getAttribute('level')) + 2;
                div.style.marginLeft = level + "em";
                div.append(arrayStructure);
                let inputRow = this.getStructureInputRow();
                div.append(inputRow);
                this.setInputVisible(Object.entries(value[0]).length, inputRow);
                div.append(this.createLabel("]"));
            } else if (typeof value === 'object') {
                titleDiv.append("{");
                const objectStructure = this.getStructureContentDiv();
                this.fillObjectTree(objectStructure, value, parentId == null ? key : parentId + "." + key);
                div.id = parentId == null ? key : parentId + "." + key;
                let level = Number(div.getAttribute('level')) + 2;
                div.style.marginLeft = level + "em";
                div.append(objectStructure);
                let inputRow = this.getStructureInputRow();
                div.append(inputRow);
                this.setInputVisible(Object.entries(value).length, inputRow);
                div.append(this.createLabel("}"));
            } else {
                titleDiv.append(valueLabel);
                // div.append(valueLabel);
            }
            content.append(div);
            titleDiv.append(this.createRemoveIcon(div, parentId, key));
            // this.addRemoveButton(nameLabel);
        }
    }
    createRemoveIcon(masterDiv: HTMLDivElement, parentKey: string, key: string): HTMLAnchorElement {
        let anchor = document.createElement('a');
        $(anchor).addClass('icon-remove');
        anchor.style.color = 'red';
        anchor.style.fontSize = 'x-large';
        anchor.style.display = 'table-cell';
        anchor.style.verticalAlign = 'middle';
        anchor.style.paddingLeft = '5px';
        anchor.style.cursor = 'pointer';
        anchor.addEventListener('click', (e) => {
            let objectId = parentKey == null ? key : parentKey + "." + key;
            let names = objectId.split('.');
            let object = this.structureForm;
            for (let i = 0; i < names.length - 1; i++) {
                object = object[names[i]];
            }
            delete object[names[names.length - 1]];
            $(masterDiv).remove();

            var numberOfFields;
            var input;
            if (parentKey == null) {
                numberOfFields = $('#structure-content')[0].children.length + 1;
                input = $('[id="structureRowInput"]')[$('[id="structureRowInput"]').length - 1];
            } else {
                numberOfFields = $('#' +parentKey).find('#structure-content').children().length
                input = $('#' +parentKey).find('[id="structureRowInput"]')[$('#' +parentKey).find('[id="structureRowInput"]').length - 1];
            }
            this.setInputVisible(numberOfFields, input);
        })
        return anchor;
    }

    private setInputVisible(numberOfFields: any, input: any) {
        if (numberOfFields >= this.maxNumOfFields) {
            input.hidden = true;
        } else {
            input.hidden = false;
        }
    }

    disable() {
        $("[id='structureRowInput']").remove();
        $('.icon-remove').remove();
    }

    setMaxField(maxField: number) {
        this.maxNumOfFields = maxField;
    }

    setMaxNest(nestLevel: number) {
        this.maxNestLevel = nestLevel;
    }

}
