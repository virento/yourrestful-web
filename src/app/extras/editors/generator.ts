import { Resource } from 'src/app/interfaces/resource';
import { RecordsCreate } from './records-create';

declare var $: any;

export class Generator extends RecordsCreate {

    protected readonly BASE_PREFIX = "base_";

    constructor() {
        super();
    }

    private inputsBase: any[] = [];

    initialize(resource: Resource, div: string) {
        resource['id'] = 'long';
        super.initialize(resource, div);
    }

    addInput(div, key: string, value: string) {
        const row = document.createElement('div');
        $(row).addClass('form-group');
        row.style.marginBottom = '0.5em';
        $(row).addClass('row');
        $(row).addClass('align-items-center');
        row.style.width = '100%';
        const label = document.createElement('label');
        // $(label).addClass('col-2');
        $(label).text(key.substring(key.lastIndexOf('.') + 1) + ":");
        $(label).prop('for', this.INPUT_PREFIX + key)
        $(row).append(label);
        this.addInputToGroup(row, key, value.toLowerCase());
        $(div).append(row);
    }

    addInputToGroup(group, key, inputType) {
        const input = document.createElement('input');
        const div = document.createElement('div');
        $(div).addClass('col');
        this.inputs.push(input);
        $(input).prop('id', this.INPUT_PREFIX + key)
        $(input).prop('type', 'text');
        $(input).addClass('form-control');
        $(input).addClass('form-control-sm');
        input.style.minWidth = '320px';

        let inputBase = this.getBaseWithName(this.BASE_PREFIX + key);
        if (inputType === 'date') {
            let baseLabel = this.createLabel('base');
            baseLabel.style.marginLeft = '1em';
            // $(baseLabel).addClass('col-2');
            $(group).append(baseLabel);
            if (inputBase == null) {
                inputBase = document.createElement('input');
                inputBase.value = '0'
                inputBase.style.marginLeft = '1em';
                inputBase.style.width = '180px';
                this.inputsBase.push(inputBase);
                $(inputBase).prop('id', this.BASE_PREFIX + key)
                $(inputBase).prop('type', 'text');
                $(inputBase).addClass('form-control');
                $(inputBase).addClass('form-control-sm');
                // $(input).addClass('col');
            }
            input.style.minWidth = '70px';
            group.append(inputBase);
        }
        switch (inputType) {
            case 'long':
            case 'double':
                input.value = 'n + (2 * n)';
                break;
            case 'text':
                input.value = '{random} {n}';
                break;
            case 'date':
                input.value = '+1M';
                inputBase.value = 'now()';
                break;
            case 'boolean':
                input.value = '(n * 3)%2 == 0 ';
                break;
        }
        let expressionLabel = this.createLabel('expression');
        expressionLabel.style.marginLeft = '1em';
        // $(expressionLabel).addClass('col-2');
        $(group).append(expressionLabel);
        div.append(input);
        group.append(div);
    }

    getInputsAsObject() {
        let object = {};
        this.inputs.forEach(element => {
            let key = element.id;
            let value = element.value;
            let baseValue = this.getBaseWithName(this.BASE_PREFIX + key.substring(this.INPUT_PREFIX.length));
            if (baseValue !== undefined && baseValue !== null) {
                value = baseValue.value + ";" + value;
            }
            this.putObjectToMap(object, key.substring(this.INPUT_PREFIX.length), value);
        });
        return object;
    }


    getBaseWithName(inputName: any): any {
        let input: any;
        for (input in this.inputsBase) {
            input = this.inputsBase[input];
            if (input.id === inputName) {
                return input;
            }
        }
    }
}
