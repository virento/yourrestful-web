import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recordPipe'
})
export class RecordPipePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let obj = JSON.parse(JSON.stringify(value));
    let text = new String();
    let id = 'id: ' + obj['id'] + ', ';
    if (args[0] == 'short') {
      return id;
    }
    obj['links'] = undefined;
    obj['id'] = undefined;
    text = JSON.stringify(obj).replace(/\":/gm, ': ').replace(/\"/gm, '').replace(/,/gm, ', ');
    text = text.substring(1, text.length - 1);
    return id + text;
  }

}
