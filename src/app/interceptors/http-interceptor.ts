import { Injectable } from '@angular/core';
import { HttpInterceptor as AngularHttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AuthenitcationService } from '../services/authenitcation.service';


@Injectable()
export class HttpInterceptor implements AngularHttpInterceptor {

    constructor(
        private authenticationService: AuthenitcationService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        let currentUser = this.authenticationService.getCurrentUserValue;
        if (currentUser && currentUser.token) {
            req = req.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + currentUser.token
                }
            });
        }
        return next.handle(req).pipe(map((event: HttpEvent<any>) => {
            return event;
        }),
            catchError((err) => {
                console.debug("Error [" + err.error.errorCode + "] + " + err.error.description);
                if (err.error.status === 403) {
                    this.authenticationService.logout();
                } else if (err.error.errorCode === 'TOKEN_EXPIRED') {
                    console.debug("Requesting new token...");
                    currentUser.token = null;
                    return this.authenticationService.loginByRefreshToken(currentUser).toPromise()
                    .then(value => {
                        return this.intercept(req, next).toPromise();
                    })
                    .catch(error => {
                        this.authenticationService.logout();
                    });
                }
                return throwError(err);
            }));
    }

}
