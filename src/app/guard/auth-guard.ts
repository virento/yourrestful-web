import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenitcationService } from '../services/authenitcation.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authorizationService: AuthenitcationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        const currentUser = this.authorizationService.getCurrentUserValue;
        if (currentUser) {
            if (currentUser.refreshToken != null && currentUser.expireDate < new Date(Date.now())) {
                currentUser.token = null;
                console.log('User refresh token is expired, requesting...');
                return this.authorizationService.loginByRefreshToken(currentUser).toPromise().then(
                    _response => {
                        let newUserAuthentication = this.authorizationService.getCurrentUserValue;
                        return newUserAuthentication && newUserAuthentication.expireDate > new Date(Date.now());
                    })
                    .catch(error => {
                        console.error(error);
                        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } })
                        return false;
                    });
            } else {
                return true;
            }
        }

        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } })
        return of(false);
    }

}
