import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { NavLink } from '../component/dashboard/dashboard-basic/nav-link';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  private pushSubject: Subject<NavLink[]> = new BehaviorSubject<NavLink[]>(null);

  constructor() { }

  get pushSubject$() {
    return this.pushSubject.asObservable();
  }

  pushNavLink(navLink: NavLink[]) {
    this.pushSubject.next(navLink);
  }

}
