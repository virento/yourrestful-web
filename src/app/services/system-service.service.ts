import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as Globals from '../global';
import { Paginable } from '../interfaces/common/paginable';
import { UsersCriteria, UsersCriteriaClass } from '../interfaces/criteria/UsersCriteria';
import { User, UserToken } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class SystemServiceService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getParameterValue(code: string): any {
    return this.httpClient.get<any>(Globals.parseUrl(Globals.SYSTEM_PARAMETER, [code])).pipe(map(obj => obj.value));
  }

  findParameters(code: string, paginable?: Paginable): any {
    let params = this.getPaginableQuery(paginable);
    if (code) {
      if (params.length === 0) {
        params = "?";
      } else {
        params += "&";
      }
      params += "code=" + code;
    }
    return this.httpClient.get<any>(Globals.SYSTEM_PARAMETERS + params).pipe();
  }

  findUsers(criteria: UsersCriteria): any {
    let query = UsersCriteriaClass.createQuery(criteria);
    return this.httpClient.get<any>(Globals.SYSTEM_USERS + query).pipe();
  }

  modifyParameter(code: string, value: string): any {
    return this.httpClient.patch<any>(Globals.parseUrl(Globals.SYSTEM_PARAMETER, [code]), { "value": value }).pipe();
  }

  modifyUser(user: User): any {
    return this.httpClient.patch<any>(Globals.parseUrl(Globals.SYSTEM_USER, [user.username]), user).pipe();
  }

  private getPaginableQuery(paginable: Paginable): string {
    let query = "";
    if (paginable) {
      if (paginable.maxResult) {
        query += "?maxResult=" + paginable.maxResult;
      }
      if (paginable.page) {
        if (query.length != 1) {
          query += "&";
        }
        query += "page=" + paginable.page;
      }
    }
    return query;
  }

}
