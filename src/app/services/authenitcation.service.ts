import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as  Globals from '../global';
import { RegisterForm } from '../interfaces/register-form';
import { LoginForm } from '../interfaces/login-form';
import { UserToken } from '../interfaces/user';
import { Observable, BehaviorSubject, throwError, ReplaySubject, AsyncSubject } from 'rxjs';
import { map, catchError, combineAll } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenitcationService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<UserToken>;

  private refreshSubject: AsyncSubject<any> = null;

  private tokenRequest = null;

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) {
    const user: UserToken = JSON.parse(localStorage.getItem('user'));
    if (user) {
      user.expireDate = new Date(user.expireDate);
      this.currentUserSubject = new BehaviorSubject<any>(user);
    } else {
      this.currentUserSubject = new BehaviorSubject<any>(null);
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get getCurrentUserValue(): UserToken {
    return this.currentUserSubject.value;
  }

  prepareCurrentUser(): boolean {
    let user = this.getCurrentUserValue;
    if (user === null) {
      return true;
    }
    if (user.expireDate > new Date(Date.now())) {
      return true;
    }
    user.token = null;
    this.loginByRefreshToken(user).toPromise()
      .then(() => {
        return true;
      })
      .catch(() => {
        this.logout();
        return false;
      });
  }

  public getProfile() {
    return this.authenticatedCall(() => this.httpClient.get<any>(Globals.USER_INFO, { observe: 'response' }));
  }

  registerUser(registerForm: RegisterForm) {
    return this.httpClient.post<any>(Globals.AUTHENTICATION_REGISTER, registerForm, { observe: 'response' }).pipe();
  }

  loginUser(loginForm: LoginForm) {
    return this.httpClient.post<any>(Globals.AUTHENTICATION_LOGIN, loginForm, { observe: 'response' }).pipe(map(data => this.initAuthenticatedUser(data), catchError(err => throwError(err))));
  }

  private initAuthenticatedUser(data) {
    this.tokenRequest = null;
    console.debug('Init authenticated user')
    console.debug(data);
    let userData = {
      token: data.body.access_token,
      refreshToken: data.body.refresh_token,
      expireDate: new Date(Date.now() + data.body.expire_in)
    };
    localStorage.setItem('user', JSON.stringify(userData));
    this.currentUserSubject.next(userData);
    this.requestUserInfo();
    return userData;
  }

  loginByRefreshToken(user: UserToken): Observable<any> {
    if (this.refreshSubject !== null) {
      return this.refreshSubject;
    }
    this.refreshSubject = new AsyncSubject<any>();
    const refreshToken = user.refreshToken;
    user.refreshToken = null;
    let params = "?grant_type=refresh_token&refresh_token=" + refreshToken;
    console.log('createing request');

    this.httpClient.post<any>(Globals.AUTHENTICATION_LOGIN + params, null, { observe: 'response' })
      .pipe(map(data => this.initAuthenticatedUser(data))).subscribe(data => {
        console.log('data next');
        this.refreshSubject.next(data);
      }, errr => {
        this.refreshSubject.error(errr);
      }, () => {
        console.log('clearing');
        this.refreshSubject.complete();
        this.refreshSubject = null;
      });

    return this.refreshSubject;
  }

  requestUserInfo() {
    let user: UserToken = this.getCurrentUserValue;
    if (user) {
      this.httpClient.get<any>(Globals.USER_INFO).pipe().subscribe(data => {
        user.username = data.username;
        user.firstName = data.firstName;
        user.lastName = data.lastName;
        user.email = data.email;
        user.registrationDate = data.registrationDate;
        user.status = data.status;
        user.type = data.type;
        this.currentUserSubject.next(user);
        localStorage.setItem('user', JSON.stringify(user));
      });
    }
  }

  verifyEmail(token: string, username: string) {
    let params = "?token=" + token + "&username=" + username;
    return this.httpClient.get<any>(Globals.VERIFICATION_EMAIL + params, { observe: 'response' }).pipe();
  }

  resetPassword(emailUsername: string) {
    return this.httpClient.post<any>(Globals.RESET_PASSWORD, { email: emailUsername }, { observe: 'response' }).pipe();
  }

  submitNewPassword(token: string, username: string, password: string) {
    let params = "?token=" + token + "&username=" + username;
    return this.httpClient.post<any>(Globals.SUBMIT_REST_PASSWORD + params, { password: password }, { observe: 'response' }).pipe();
  }

  logout() {
    localStorage.removeItem('user');
    this.currentUserSubject.next(null);
    this.router.navigate(['/home']);
  }

  /**
   * This method will invoke callback only when user is authenticated.
   * If current token is expired, this method will wait until user is authenticated
   * @param callback Mathod to invoke
   */
  public authenticatedCall(callback: () => Observable<any>): Observable<any> {
    const subject: ReplaySubject<any> = new ReplaySubject<any>(1);
    const observabe: Observable<any> = subject.asObservable();
    let subscribtion = { object: null, invoked: false };
    subscribtion['object'] = this.currentUser.subscribe(data => {
      if (subscribtion.invoked) {
        subscribtion.object.unsubscribe();
        return;
      }
      console.log('New user, trying new call' + callback)
      if (data == null || data.token == null) {
        console.debug("skipping..." + callback)
        return;
      }
      subscribtion.invoked = true;
      callback().subscribe({
        next: data => {
          subject.next(data)
          subject.complete();
          if (subscribtion.object) {
            console.debug('[next] Unsubscribing authentication call' + callback);
            subscribtion.object.unsubscribe();
          }
        },
        error: error => {
          subject.error(error)
          subject.complete();
          if (subscribtion.object) {
            console.debug('[next] Unsubscribing authentication call' + callback);
            subscribtion.object.unsubscribe();
          }
        }
      });
    });
    return observabe;
  }
}
