import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenitcationService } from './authenitcation.service';
import * as Globals from '../global';
import { Paginable } from '../interfaces/common/paginable';
import { Observable } from 'rxjs';
import { ResourceForm } from '../interfaces/resource/resource-form';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  constructor(
    private httpClient: HttpClient,
    private authenticationService: AuthenitcationService
  ) { }

  public getResourceList(paginable?: Paginable): Observable<any> {
    let query = this.getPaginableQuery(paginable);
    return this.authenticationService.authenticatedCall(() => this.httpClient.get<any>(Globals.RESOURCE_LIST, { observe: 'response' }));
  }

  private getPaginableQuery(paginable: Paginable): string {
    let query = "";
    if (paginable) {
      if (paginable.maxResult) {
        query += "?maxResult=" + paginable.maxResult;
      }
      if (paginable.page) {
        if (query.length != 1) {
          query += "&";
        }
        query += "page=" + paginable.page;
      }
    }
    return query;
  }

  public postNewResource(resource, generate: {}) {
    if (generate != null) {
      resource.generation = generate;
    }
    return this.authenticationService.authenticatedCall(() => this.httpClient.post<any>(Globals.RESOURCE_CREATE, resource, { observe: 'response' }));
  }

  public getResource(resourceUrl: String): any {
    return this.authenticationService.authenticatedCall(() => this.httpClient.get<any>(Globals.RESOURCE_GET + '/' + resourceUrl, { observe: 'response' }));
  }

  public getResourceRecords(resourceUrl: string, paginable?: Paginable) {
    let query = this.getPaginableQuery(paginable);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.get<any>(Globals.RESOURCE_RECORDS_LIST + "/" + resourceUrl + query, { observe: 'response' })
    );
  }

  public postNewResourceRecord(resourceUrl: string, record: {}) {
    let url = Globals.parseUrl(Globals.RESOURCE_CREATE_RECORD, [resourceUrl]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.post<any>(url, record, { observe: 'response' })
    ).pipe();
  }

  public getResourceStructure(resourceUrl: string) {
    let url = Globals.parseUrl(Globals.RESOURCE_STRUCTURE_GET, [resourceUrl]);
    return this.authenticationService.authenticatedCall(() => this.httpClient.get<any>(url, { observe: 'response' }));
  }

  public patchResource(resourceUrl: string, resource) {
    let url = Globals.parseUrl(Globals.RESOURCE_MODIFY, [resourceUrl]);
    return this.authenticationService.authenticatedCall(() => this.httpClient.patch<any>(url, resource, { observe: 'response' }));
  }

  public getResourceShares(resourceUrl: string, paginable?: Paginable) {
    let query = this.getPaginableQuery(paginable);
    let url = Globals.parseUrl(Globals.RESOURCE_SHARE_LIST, [resourceUrl]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.get<any>(url + query, { observe: 'response' }));
  }

  public postResourceShare(resourceUrl: string, username: string, email: string) {
    let body = {
      nick: username,
      email: email
    };
    let url = Globals.parseUrl(Globals.RESOURCE_SHARE, [resourceUrl]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.post<any>(url, body, { observe: 'response' }));
  }

  public removeUserShare(resourceUrl: string, username: string) {
    let url = Globals.parseUrl(Globals.RESOURCE_SHARE_REMOVE, [resourceUrl, username]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.delete(url, { observe: 'response' })
    )
  }

  public removeRecord(resourceUrl: string, recordId: string) {
    let url = Globals.parseUrl(Globals.RESOURCE_RECORD_DELETE, [resourceUrl, recordId]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.delete(url, { observe: 'response' })
    );
  }

  public removeResource(resourceUrl: string) {
    let url = Globals.parseUrl(Globals.RESOURCE_REMOVE, [resourceUrl]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.delete(url, { observe: 'response' })
    );
  }

  public generateRecords(resourceUrl: string, generate: {}) {
    let url = Globals.parseUrl(Globals.RESOURCE_GENERATE_RECORDS, [resourceUrl]);
    return this.authenticationService.authenticatedCall(
      () => this.httpClient.post<any>(url, generate, { observe: 'response' })
    );
  }
}
