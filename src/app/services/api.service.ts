import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as Globals from '../global';
import { Resource } from '../interfaces/resource';
import { ResourceRecord } from '../interfaces/resource-record';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  headers: {
    'Access-Control-Allow-Origin': "*";
    'Access-Control-Request-Method': 'GET'
  }

  constructor(
    private httpClient: HttpClient
  ) { }

  getAllRecords(): Observable<Resource> {
    return this.httpClient.get<Resource>(Globals.API_GET_ALL, {headers: this.headers}).pipe();
  }

  getRecord(id: Number): Observable<ResourceRecord> {
    return this.httpClient.get<ResourceRecord>(Globals.API_GET_RECORD + id).pipe();
  }

  postRecord(record: ResourceRecord): any {
    return this.httpClient.post<ResourceRecord>(Globals.API_POST_RECORD, record).pipe();
  }

  putRecord(record: ResourceRecord): Observable<ResourceRecord> {
    return this.httpClient.put<ResourceRecord>(Globals.API_PUT_RECORD + record.id, record).pipe();
  }

  editRecord(record: ResourceRecord): any {
    return this.httpClient.patch<any>(Globals.API_PATCH_RECORD + record.id, record).pipe();
  }
 
  getResourceStructure() {
    return this.httpClient.get<any>(Globals.API_STRUCTURE, { observe: 'response' });
  }
}
