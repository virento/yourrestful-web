import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './component/main/main-page/main-page.component';
import { LoginPageComponent } from './component/basic/login-page/login-page.component';
import { RegistrationPageComponent } from './component/basic/registration-page/registration-page.component';
import { AuthGuard } from './guard/auth-guard';
import { DashboardComponent } from './component/dashboard/dashboard/dashboard.component';
import { ResourceCreateComponent } from './component/dashboard/resource-create/resource-create.component';
import { ResourceDetailsComponent } from './component/dashboard/resource-details/resource-details.component';
import { RecordsModifyComponent } from './component/dashboard/records-modify/records-modify.component';
import { DashboardBasicComponent } from './component/dashboard/dashboard-basic/dashboard-basic.component';
import { ProfilePageComponent } from './component/basic/profile-page/profile-page.component';
import { SharesModifyComponent } from './component/dashboard/shares-modify/shares-modify.component';
import { PasswordResetComponent } from './component/basic/password-reset/password-reset.component';
import { GeneratorInformationComponent } from './component/basic/generator-information/generator-information.component';
import { ApiInformationComponent } from './component/basic/api-information/api-information.component';
import { AdminGuard } from './guard/admin.guard';
import { AdminDashboardComponent } from './component/admin/admin-dashboard/admin-dashboard.component';
import { AdminParametersComponent } from './component/admin/admin-parameters/admin-parameters.component';
import { AdminUsersComponent } from './component/admin/admin-users/admin-users.component';
import { AdminDashboardBasicComponent } from './component/admin/admin-dashboard-basic/admin-dashboard-basic.component';
import { LoggedGuard } from './guard/logged.guard';
import { GuestGuard } from './guard/guest.guard';


const routes: Routes = [
  {
    path: 'home', component: MainPageComponent, children: [
      { path: 'edit', component: MainPageComponent },
      { path: 'get-all', component: MainPageComponent },
      { path: 'get', component: MainPageComponent },
      { path: 'new', component: MainPageComponent },
    ]
  },
  { path: 'login', component: LoginPageComponent, canActivate: [GuestGuard] },
  { path: 'verify', component: LoginPageComponent },
  { path: 'register', component: RegistrationPageComponent, canActivate: [GuestGuard] },
  { path: 'profile', component: ProfilePageComponent },
  { path: 'resetPassword', component: PasswordResetComponent },
  { path: 'newPassword', component: PasswordResetComponent },
  { path: 'generator', component: GeneratorInformationComponent },
  { path: 'api', component: ApiInformationComponent },
  {
    path: 'dashboard', component: DashboardBasicComponent, canActivate: [AuthGuard], children: [
      { path: '', component: DashboardComponent },
      { path: 'resource/new', component: ResourceCreateComponent },
      { path: 'resource/:id', component: ResourceDetailsComponent },
      { path: 'records/:id/modify', component: RecordsModifyComponent },
      { path: 'resource/:id/modify', component: ResourceCreateComponent },
      { path: 'share/:id/modify', component: SharesModifyComponent }
    ]
  },
  {
    path: 'admin', component: AdminDashboardBasicComponent, canActivate: [AdminGuard], children: [
      { path: 'dashboard', component: AdminDashboardComponent },
      { path: 'parameters', component: AdminParametersComponent },
      { path: 'users', component: AdminUsersComponent }
    ]
  },
  { path: '', redirectTo: 'home', pathMatch: 'full', }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
